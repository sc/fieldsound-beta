# Installation Guide

This repository countain the guide and the script to run a raspberry to monitor a field
There is two main options : install the homemade raspbian image, in that case 

## Install raspbian from scratch

### Downoad and clone raspbian

- Download raspbian from: https://www.raspberrypi.org/downloads/raspbian/ 

We advice to use a lite version, which will need a SD card of at least: 2.2Go

```bash
dd bs=4M if=2019-09-26-raspbian-buster-lite.img of=/dev/mmcblk0 conv=fsync status=progress
## it often happens that the pi doesn't boot because the image has been badly copied.
```

Once this is done you can mount the partition `/root` and `/boot`  in your computer

### Configure the Pi

Then if you want to use the wifi you will need to add a file `wpa_supplicant.conf` in the `/boot/` partition of the raspberry and enable `ssh`.

A quick way to add `wpa_supplicant.conf` (when doing your install from a linux/debian like system)

```bash
echo "country=ES
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid="WIFI NAME" #replace the name of the network where the pi will be connected
    psk="PASSWORD" #replace with the password to connect to the network where the pi will be connected
    key_mgmt=WPA-PSK
}" > /media/$USER/boot/wpa_supplicant.conf
```

And to enable ssh:
```bash
touch /media/$USER/boot/ssh
```

Activate the  1-wire interface

Add in the file `/media/$USER/boot/config.txt` the following line:

```
dtoverlay=w1-gpio #default pin is pin4, if another pin 'x' is needed change with:dtoverlay=w1-gpio,gpiopin=x
```
((=> It looks like debian buster changed that. to be checked (I had to manually activate w1-gpui using raspi-config on the pi and it worked, not about the not, I guess the problem came from the fact that I wrote inside CMDLINE and not CONFIG!) ))

Once the configuration is done you can insert the SD card in the raspberry and plug the power cable to boot. From this stage, if the wifi is well configured you should see the raspberry connected to your network and find its IP. By default the hostname of the raspberry is set to "raspberrypi" so you should be able to connect by ssh using:

```bash
ssh pi@raspberrypi
```
The default password of raspbian is `raspberry`

### Install the program needed

At this stage your are connected to the raspberry pi, a check for update can be a good option :
```bash
sudo apt update
sudo apt upgrade
```
And changing the hostname is also recommanded (you can use the raspi-config)

To use the pi as we want it we will need: 
- git: to download our scripts,
- pip: to install the telgram bot library,
- telegram python bot (cf https://python-telegram-bot.org/)   
- pigpio library, to speak with DHT11

```
sudo apt-get install python3-pip git pigpio
sudo pip3 install python-telegram-bot
```

Install DHT11 drivers (humidity sensors):

```bash
wget http://abyz.me.uk/rpi/pigpio/code/DHTXXD.zip
unzip DHTXXD.zip
gcc -Wall -pthread -o DHTXXD test_DHTXXD.c DHTXXD.c -lpigpiod_if2
```

### Install homemade scripts:

Clone the repository:

```bash
git clone https://framagit.org/sc/fieldsound-beta
```

Change the bot key in the file `/home/pi/fieldsound-beta/iotestbot.py` with the good one

Install the script that monitor and store data:

```bash
cd fieldsound-beta
sudo bash install.sh
cp ../DHTXXD .
```

Now the pi can be reboot and if you plug a DS18B20 on the pin 4 and DHT11 (or 22) on pin 16 and 26 you should be able to ask the telegram bot to help you


## Install raspbian from the custom image:

```	
dd bs=4M if=customImage.img of=/dev/mmcblk0 conv=fsync status=progress
```

### Update the Wifi configuration:
Open the file `/etc/wpa_supplicant/wpa_supplicant.conf` and update the network name (SSID) and the WPA key

### Update the telegram bot configuration :
Change the bot key in the file `/home/pi/fieldsound-beta/iotestbot.py` with the good one


## Plug sensors:

once all is installed we need to plug at least two DS28B20 (temperature) and two DHT11 (humidity).
To do that you will need the 3v3 power which is the first pin on the left when the SD card is up and for the ground we will use the 3th pin on the right when SD's up. 
Then you can plug the one-wire pin which by default is the pin 4, ie the 4th pin on the left when SD is up. For the DHT we will use the pin 26 and 16 which are respectively the 2nd pin starting from the bottom on the left and the 3rd pin starting from the bottom on the right.

Both the DS18b20 and DHT11 need pull up resistors, the DS18B20 can be plugged in series so just one is need but the DHT11 can't so you'll need to for them. I use 10k ohm for the DHT11 and 4k7 ohms for DS18B20 thought the 10kOhm works also for the DS18B20 and probably a much wider range of resistance could work, but this should be tested.

To wire the DS18B20 you can check this image: ![DS18B20](https://cdn.pimylifeup.com/wp-content/uploads/2016/03/Raspberry-Pi-Temperature-Sensor-Diagram-v2.png)

To wire the DHT11 you can check this image, just change the blue cable and put it on pin 26 or 16: ![DHT11](http://www.circuitbasics.com/wp-content/uploads/2015/12/How-to-Setup-the-DHT11-on-the-Raspberry-Pi-Four-pin-DHT11-Wiring-Diagram-768x334.png)


All the pin numbers and useage are here:https://pinout.xyz/)

(write some quick explaination about the telegram bot)

And this should be good enough you can send /checksensors to the telegram bot and see what happens!


Sources:
- https://pinout.xyz/pinout/1_wire#
- https://framagit.org/sc/sagrescript
- http://www.reuk.co.uk/wordpress/raspberry-pi/ds18b20-temperature-sensor-with-raspberry-pi/
- https://python-telegram-bot.org/




