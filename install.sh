#this is the path to the folder that contain the script of the repository
FSINSTALL=/home/pi/fieldsound-beta

#make all files executable
chmod a+x $FSINSTALL/monitortmp.sh
chmod a+x $FSINSTALL/monitortmpd
chmod a+x $FSINSTALL/telegrambotd

#symbolic link
ln -s $FSINSTALL/monitortmpd /etc/init.d/monitortmpd
ln -s $FSINSTALL/telegrambotd /etc/init.d/telegrambotd
ln -s $FSINSTALL/monitortmp.sh /usr/bin/monitortmp.sh 

#record and enable the daemon
systemctl daemon-reload
systemctl enable monitortmpd
systemctl enable telegrambotd 
