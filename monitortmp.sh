echo "init humidity sensors"

moist1=16
moist2=26

echo $moist1 >  /sys/class/gpio/export
sleep 1
echo $moist2 >  /sys/class/gpio/export
sleep 1
echo in > /sys/class/gpio/gpio$moist1/direction
sleep 1
echo in > /sys/class/gpio/gpio$moist2/direction

pigpiod


if [ ! -d /home/pi/fieldsound-beta/data ]; then
	mkdir -p /home/pi/fieldsound-beta/data;
fi


echo "start monitoring"

while true ; do
	alltemp=""
	for w1 in 28-00000a* ; do 
		u=$(cat /sys/devices/w1_bus_master1/$w1/w1_slave | grep t= | sed "s/t=//g" | awk '{print $10}')
		alltemp=$alltemp" "$u" "
 	done
	tdate=$(date -d "today" +"%Y%m%d%H%M%S")
	h1=$(/home/pi/fieldsound-beta/DHTXXD -g16 | awk '{print $3}')
	h2=$(/home/pi/fieldsound-beta/DHTXXD -g26 | awk '{print $3}')
	echo $tdate" "$alltemp$h1" "$h2 >> /home/pi/fieldsound-beta/data/monitortmp.csv
	sleep 10
done


