from telegram.ext import Updater
from telegram.ext import MessageHandler, Filters
from telegram.ext import CommandHandler
from telegram import ChatAction
import logging
import os,subprocess
import re

updater = Updater(token='705429666:AAElwnKeetKIh8u7WscDcFQ-4LSD6be3CaU')
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                             level=logging.INFO)
def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Fieldsound Beta: Simple bot to test humidity and temperature sensors. \n Commands : \n /checksensors see the state of all sensors \n /start see this menu \n /getip return the IP of the bot")

###Get Temperature 
def checksensors(bot, update,args):
    user_says="" 
    if(len(args)>0):
        user_says=args[0]
    if(user_says == "dia"):
        bot.send_message(chat_id=update.message.chat_id, text="ahora te mando el grafico de la temperatura de las ultimas 24 horas" ,timeout=240)
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.UPLOAD_PHOTO)
        bot.send_photo(chat_id=update.message.chat_id, photo=open("../images/twentyfor.png",'rb'),timeout=250)
    elif(user_says == "mes"):
        bot.send_message(chat_id=update.message.chat_id, text="ahora te mando el grafico de la temperatura del ultimo mes" ,timeout=240)
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.UPLOAD_PHOTO)
        bot.send_photo(chat_id=update.message.chat_id, photo=open("../images/mes.png",'rb'),timeout=250)
    else:
        bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        for line in open('/home/pi/fieldsound-beta/data/monitortmp.csv','rb'): pass #go to the last line of the file
        allt=str(line.decode("utf-8")).rstrip().split(' ')
        #bot.send_message(chat_id=update.message.chat_id, text=str(allt),timeout=240)
        bot.send_message(chat_id=update.message.chat_id, text="date: "+allt[0]+" temp " + str(float(allt[2])/1000) + "C , "+ str(float(allt[3])/1000) +"C , "+ allt[4] +"% humidity, "+ allt[5] +"% humidity." ,timeout=240)


def last(bot, update):
        bot.send_photo(chat_id=update.message.chat_id, photo=open("../images/last.png",'rb'))

def getip(bot, update):
        myip=str(subprocess.check_output(["curl" ,"-4","http://icanhazip.com/" ]))
        bot.send_message(chat_id=update.message.chat_id, text="actual sagregang ip: "+myip)

def takepics(bot, update):
    bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
    #bot.send_message(chat_id=update.message.chat_id, text="Chupame la pija marcos...")
    bot.send_message(chat_id=update.message.chat_id, text="Tengo que preguntar a chepapi si me puede sacar una foto ! wait...")
    #bot.send_message(chat_id=update.message.chat_id, text="chepapi parce muertoe :(((")
    bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.RECORD_VIDEO)
    os.system("bash makeNgetPicture.sh")
    bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.RECORD_VIDEO)
    bot.send_message(chat_id=update.message.chat_id, text="Hecho, ahora te la mando.")
    bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.UPLOAD_PHOTO)
    bot.send_photo(chat_id=update.message.chat_id, photo=open("annotated.png",'rb'),timeout=250)
    os.remove('annotated.png')


start_handler = CommandHandler('start', start) #command /start
last_handler = CommandHandler('last', last)
getip_handler = CommandHandler('getip', getip)
checksensors_handler = CommandHandler('checksensors', checksensors,pass_args=True)
takepics_handler = CommandHandler('takepics', takepics)
dispatcher.add_handler(takepics_handler)
dispatcher.add_handler(getip_handler)
dispatcher.add_handler(checksensors_handler)
dispatcher.add_handler(start_handler)
updater.start_polling()
updater.idle()


